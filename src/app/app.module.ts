/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';


import {MatListModule, MatTooltipModule, MatSlideToggleModule, MatExpansionModule, MatSelectModule,  MatCardModule, MatFormFieldModule, MatGridListModule, MatSnackBarModule, MatButtonModule, MatCheckboxModule, MatInputModule, MatButtonToggleModule, MatTabsModule} from '@angular/material';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {TemplatePrepareComponent} from './template-prepare/template-prepare.component';
import {TemplateChooseComponent} from './template-choose/template-choose.component';
import {AboutReporterComponent} from './about-reporter/about-reporter.component';
import {HelpComponent} from './help/help.component';
import {DataService} from './data.service';
import { SettingsComponent } from './settings/settings.component';

import {MarkdownToHtmlModule} from 'markdown-to-html-pipe';
import { OptionHelperComponent } from './option-helper/option-helper.component';
import { ExamplesSelectorComponent } from './examples-selector/examples-selector.component';
import { TemplatesManagerComponent } from './templates-manager/templates-manager.component';

@NgModule({
    declarations: [
        AppComponent,
        TemplatePrepareComponent,
        TemplateChooseComponent,
        AboutReporterComponent,
        HelpComponent,
        SettingsComponent,
        OptionHelperComponent,
        ExamplesSelectorComponent,
        TemplatesManagerComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        MatFormFieldModule,
        MatButtonToggleModule,
        MatTabsModule,
        MatSnackBarModule,
        MatGridListModule,
        MatCardModule,
        MatSelectModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatTooltipModule,
        MatSlideToggleModule,
        MarkdownToHtmlModule,
        MatExpansionModule,
        MatListModule
    ],
    exports: [],
    providers: [DataService],
    bootstrap: [AppComponent],
    entryComponents: [

    ] // entryComponents is used for components created on the fly (e.g. the SnackBars)
})
export class AppModule {}
