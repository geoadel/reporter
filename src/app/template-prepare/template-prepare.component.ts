/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */

import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';

@Component({
    selector: 'app-template-prepare',
    templateUrl: './template-prepare.component.html',
    styleUrls: ['./template-prepare.component.css']
})
export class TemplatePrepareComponent implements OnInit {

    newTitle: string = "";
    templateTitle: string = "";
    templateText: string;

    isVisibleTemplateEdit: boolean = false;

    constructor(public dataService: DataService) {}

    ngOnInit() {
        if (localStorage.getItem("Last template")) {
            this.dataService.setTemplateSource(this.dataService.templateSource = localStorage.getItem("Last template"));
        } else {
            let defaultText = "Welcome to the Reporter!"
            
            +"Dear #name, thank you [Thanks*so much|so unbelievably much|a thousand times] for opening this page. If you want to get a brief overview, visit the Help-tab above or load an example below."

            this.dataService.setTemplateSource(defaultText);
        }
    }

    setText(text: string) {
        this.dataService.setTemplateSource(text);
    }

    toggleVisibilityTemplateEdit() {
        this.isVisibleTemplateEdit = !this.isVisibleTemplateEdit;
    }



}
