/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */

export class Variable {
    constructor(id: number, tag: string) {
        this.id = id;
        this.tag = tag;
    }

    // the char position of the first occurence of the variable
    id: number;

    // the tag, using all lower case letters
    tag: string;
    
    // the string to replace the tag
    replacement: string = "";

    // isDisplayedFully is used to toggle the display state of the option
    isDisplayedFully: boolean = true;

    isActive: boolean = false;
    
    occursInText : boolean = true;
    

    getReplacement(): string {
        let result = "";
        if(this.replacement){
            result = this.replacement;
        }

        if (this.isActive && result.length > 0) {
            //if is active, mark the first word (markdown)
            if(result.indexOf(" ") > 0){
                result = "*" + result.substring(0, result.indexOf(" ")) +"* " +result.substring(result.indexOf(" "));
            } else {
                result = result.trim();
                result = "*" + result +"*";
            }
        }
                
        return result;
    }

    setReplacement(text: string) {
        this.replacement = text;
    }

    toggleDisplay() {
        // console.log("Toggling visiblity of option "+this.id+ " to " + this.isDisplayedFully);
        this.isDisplayedFully = !this.isDisplayedFully;
    }


    identicalToString(text: string): boolean {
        return this.tag === text;
    }

}


