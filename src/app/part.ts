/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */

export class Part {
    constructor(id: number, part : any, isOption : boolean, isText : boolean, isNewline : boolean) {
        this.id = id;
        this.part = part;
        
        this.isOption = isOption;
        this.isText = isText;
        this.isNewline = isNewline;
    }
    
    id : number = 0;
    
    part : any;
    
    isOption : boolean;
    isText : boolean;
    isNewline : boolean;
}