import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamplesSelectorComponent } from './examples-selector.component';

describe('ExamplesSelectorComponent', () => {
  let component: ExamplesSelectorComponent;
  let fixture: ComponentFixture<ExamplesSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamplesSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamplesSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
