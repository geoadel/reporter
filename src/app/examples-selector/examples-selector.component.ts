/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */


import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-examples-selector',
  templateUrl: './examples-selector.component.html',
  styleUrls: ['./examples-selector.component.css']
})
export class ExamplesSelectorComponent implements OnInit {

    constructor(public dataService: DataService) {}
    
  ngOnInit() {
  }


    exampleTitle:string = "";

  exampleTitles: Array<string> = ["Report", "Happy Birthday"];
    examples: Array<string> = ["#name arrived [Punctuality * on time | too early | too late] to the conversation and appeared in <clothing style * and% decent | inconspicuous | elegant | athletic | provocative | simple | unkempt | scruffy > clothing.\n\n" +
        "{Gender * Man: His | Woman: Her} hair was [hair * neat | thin | stringy | unkempt | tousled ]. {Other Peculiarities*No:|Yes:#peculiarities}\n\n" +
        "{Gender*Man:His|Woman:Her} attitude would best be described as <attitude * and % upright | easy-going | flirtatious|  unconstrained | petite | threatening | curious | stiff | inconspicuous | uptight>.",
        "Dear #name,\n\n\ncongratulations to your #Age birthday.\n\n\nWe wish {Familiy-status*Single:You|Partner:You and your partner|Partner and kids:You, your partner and kids|Single parent: You and the kiddies} a day [Wishes* full of joy, celebration and heartwarming memories| filled with a good mix of relaxation and inspiration|warmly spent together with family and friends].\n\n\nAll the best {To whom*Single:|Partner:You and your partner|Partner and kids:to your family|Single parent:You and your children} from [Signers* your colleagues!| the whole headquarter! | the IT team!]"
    ];
    
        loadExample(title : string) {
        console.log("Trying to load example " + title);
        if (title && title.length > 0) {
            this.dataService.templateSource = this.examples[this.exampleTitles.indexOf(title)];
            this.dataService.templateSourceChanged();
        }
    }



}
