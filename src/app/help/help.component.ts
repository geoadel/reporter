/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */

import {Component, OnInit} from '@angular/core';
import {Option} from '../option';
import {Variable} from '../variable';

@Component({
    selector: 'app-help',
    templateUrl: './help.component.html',
    styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {

    examples: Array<Option> = [];
    exampleVariable: Variable;

    exampleHeadline : string = "## headline";
    exampleBoldtext : string = "This is **boldly** emphasized text.";
    exampleQuote : string = "> This is a quotation, meaning it will be indented and multiline if too long for one line.";

    constructor() {}

    ngOnInit() {
        let option: Option = new Option(1, ["duck", "parrot", "kakapo"], "");
        option.title = "Birds";
        this.examples.push(option);

        option = new Option(2, ["The group therapy focused depression.", "Anxiety-related experiences were shared.", ""], "");
        option.isNamed = true;
        option.title = "GroupTherapy";
        option.setChoicesNames(["Depression", "Anxiety","None"]);
        this.examples.push(option);

        option = new Option(3, ["chocolate", "white chocolate", "chocolate icecream", "hot chocolade"], "");
        option.multipleChoiceConnector = " and ";
        option.isMultipleChoice = true;
        option.title = "Favourite foods";
        this.examples.push(option);
        
        this.exampleVariable = new Variable(4, "name");
    }



}
