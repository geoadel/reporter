/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */



import {Injectable} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser'

import {Option} from './option';
import {Variable} from './variable';
import {Part} from './part';

@Injectable()
export class DataService {
    constructor(private sanitizer: DomSanitizer) {}

    templateSource: string = "";

    markdownText: string = "";

    options: Array<Option> = [];

    variables: Array<Variable> = [];

    optionsAndVariablesSorted: Array<any> = [];

    finalText: string = "";

    textParts: Array<Part> = [];

    // indicates whether the templateSource has changed
    templateSourceHasChanged: boolean = false;

    getTemplateSource(): string {
        // console.log("Returning template source.");
        return this.templateSource;
    }

    setTemplateSource(text: string) {
        this.templateSource = text;
        this.update();
        this.updateMarkdown();
    }

    update() {
        localStorage.setItem("Last template", this.templateSource);

        this.templateSourceHasChanged = true;

        this.options = [];
        this.variables = [];

        this.parseOptions();
        this.parseVariables();

        this.getOptionsAndVariablesSortedByPosition();

        this.textParts = this.getTextParts();

        this.templateSourceHasChanged = false;
    }

    activate(option: Option) {
        for (let opt of this.options) {
            opt.isActive = (option.id === opt.id);
        }
        option.isActive = true;
    }

    parseOptions() {
        this.options = [];
        // regex to capture group of either named{}, unnamed[] or multiple choice<> options
        var regexOptions = /(\{.*?\})|(\[.*?\])|(\<.*?\>)/g;

        // short term result variable to loop through regex exec results
        var result;

        while (result = regexOptions.exec(this.templateSource)) {
            this.createOption(result);
        }
        // console.log("Created options: " + JSON.stringify(this.options));
    }

    createOption(regexResult: Array<any>): Option {
        // console.log(regexResult);
        let text: string = regexResult[0];

        let textBracketsRemoved: string = text.substring(1, text.length - 1);

        let id: number = regexResult["index"];
        let choices: Array<string> = textBracketsRemoved.split("|");

        let option: Option = new Option(id, choices, text);

        // check, if option has an overall title
        if (choices[0] && choices[0].length > 0 && choices[0].indexOf("*") > 0) {
            let title = choices[0].substring(0, choices[0].indexOf("*"));
            choices[0] = choices[0].substring(choices[0].indexOf("*") + 1);
            // update the choices
            option.setChoices(choices);
            option.title = title;
        }

        // if option is multiple choice <>, mark it
        option.isMultipleChoice = text.startsWith("<");

        // if option is multiple choice, fill the array of selected choices with false
        if (option.isMultipleChoice) {
            // filter out connectors if present
            if (choices[0] && choices[0] && choices[0].indexOf("%") > 0) {
                option.multipleChoiceConnector = choices[0].substring(0, choices[0].indexOf("%"));
                choices[0] = choices[0].substring(choices[0].indexOf("%") + 1);
            }
            for (let choice of option.getChoices()) {
                option.selectedChoices.push(false);
            }

            option.setChoices(choices);
        }

        // handle named options to separate names and choices {}
        option.isNamed = text.startsWith("{");

        if (option.isNamed) {
            let names: Array<string> = [];
            let choicesWithoutNames: Array<string> = [];
            for (let choice of choices) {
                let indexSeparator: number = choice.indexOf(":");
                if (indexSeparator >= 0) {
                    let name: string = choice.substring(0, indexSeparator);
                    names.push(name.trim());
                    choicesWithoutNames.push(choice.substring(indexSeparator + 1));
                }
            }

            if (names.length === choices.length) {
                option.choicesNames = names;
                option.setChoices(choicesWithoutNames);
            }
        }


        // console.log("Created option: " + option.choices + " and added to " + this.options.length+" other options.");
        this.options.push(option);
        return option;
    }

    /**
     * Set choice for given option, and if it is a named choice, set the named option for all other options with the identical named choice
     */
    setChoice(option: Option, index : number) {

        option.setChoice(index);
        option.isDisplayedFully = false;
        console.log("Set option " + option.id + " to minimized.");

        if (option.isNamed) {
            let name: string = option.getName(index);
            if (name) {
                // check other options (if they are named) for the named choice selected
                // and if they have a choice with the same name, set that choice for that other option, too
                for (let option_other of this.options) {
                    let i: number = 0;
                    if (option_other.isNamed && option_other.choicesNames) {
                        for (let choice_name of option_other.choicesNames) {
                            if (choice_name === name) {
                                option_other.setChoice(i);
                                option_other.isDisplayedFully = false;
                                console.log("... and set option " + option_other.id + " to minimized.");
                            } else {
                                console.log(choice_name + " was not the same as " + name);
                            }
                            i++;
                        }
                    }
                }
            }
        }
    }

    templateSourceChanged() {
        this.templateSourceHasChanged = true;
    }

    hasTemplateSourceChanged(): boolean {
        return this.templateSourceHasChanged;
    }

    parseVariables() {
        this.variables = [];

        if (this.templateSource && this.templateSource.length > 0) {
            var regexVariables = /#[äöüÄÖÜß\w]+\w*/g;

            var matches: Array<string> = this.templateSource.match(regexVariables);

            var result: Array<string> = [];
            if (matches) {
                for (let match of matches) {
                    if (result.indexOf(match) < 0) {
                        result.push(match);
                    }
                }

                for (let tag of result) {
                    let variable: Variable = new Variable(this.templateSource.indexOf(tag), tag);
                    this.variables.push(variable);
                }
            }
        }
    }


    updateMarkdown(): string {
        let text: string = "";

        // console.log("Getting text parts: " + this.getTextParts().length);

        for (let part of this.getTextParts()) {
            if (part.isOption) {
                // console.log("Get text from option "+JSON.stringify(part.part));
                let option: Option = part.part;
                if (!option.isMultipleChoice) {
                    // console.log("Replacing text of " + option.id + ": " + option.getSelectedChoiceText());
                    text = text + option.getSelectedText();
                } else {
                    // console.log("Replacing text of " + option.id + ": " + option.getMutipleChoiceText())
                    text = text + option.getSelectedText();
                }
            } else {
                text = text + part.part;
                if (part.isNewline) {
                    text = text + "\n";
                }
            }
        }

        this.markdownText = this.replaceTags(text);

        return this.markdownText;
    }


    /**
     * return options and variables in one array sorted by their position in the template source (if that is what is used as id)
     */
    getOptionsAndVariablesSortedByPosition(): Array<any> {
        let result: Array<any> = [];

        result = result.concat(this.options, this.variables);

        result.sort((a, b): number => {
            if (a.id < b.id) return -1;
            if (a.id > b.id) return 1;
            return 0;
        });

        this.optionsAndVariablesSorted = result;

        return result;
    }


    getVariables(): Array<Variable> {
        return this.variables;
    }

    /**
     * set the replacement string for a variable by searching for a variable with the given
     * tag and for that variable setting the replacement
     */
    setVariableReplacement(tag: string, replacement: string) {
        for (let variable of this.variables) {
            if (variable.identicalToString(tag)) {
                variable.setReplacement(replacement);
                // console.log("Replacing " + tag + " with " + replacement);
            }
        }
    }

    // get all parts in their corresponding type, so either string or option
    getTextParts(): Array<Part> {
        if (this.getTemplateSource().length > 0) {
            // regex for {...}, [...] - could include tags #[a-zA-Z]\w* but for now treat them separately
            var regexOptions = /(\{.*?\})|(\[.*?\])|(\<.*?\>)/g;

            // split text into parts (including options, named options and normal text)
            let parts: Array<string> = String(this.getTemplateSource()).split(regexOptions);
            // console.log("Split text into: " + JSON.stringify(parts));
            let result: Array<Part> = [];

            for (let part of parts) {
                if (part !== undefined) {
                    let isNotOption: boolean = true;

                    // if part is an option, find the corresponding option
                    for (let option of this.options) {
                        if (option.identicalToString(part) && !this.resultContainsID(result, option.id)) {
                            let newPart: Part = new Part(option.id, option, true, false, false);
                            if (result.indexOf(newPart) < 0) {
                                result.push(newPart);
                                // console.log("Adding option " + option.id + " to result.");
                                isNotOption = false;
                                break; // break the for loop to avoid adding named options several times (as their text might occur several times)
                            } else {
                                // console.log("Avoiding doubling option...");
                            }
                        }
                    }
                    if (isNotOption) {
                        // split into new lines
                        let textParts: Array<string> = part.split(/\n/g);
                        for (let textPart of textParts) {
                            if (textPart.length === 0) {
                                result.push(new Part(0, textPart, false, false, true));

                            } else {
                                result.push(new Part(0, textPart, false, true, false));
                            }
                        }
                    }
                }
            }
            return result;
        }
        return [];
    }

    resultContainsID(result: Array<any>, id: number): boolean {
        for (let part of result) {
            if (part.id) {
                // console.log("Is an option");
                if (part.id === id) {
                    return true;
                }
            }
        }
        return false;
    }

    setActive(id: number) {
        for (let option of this.options) {
            option.isActive = (id === option.id);
            // console.log("Option "+option.id + " is active: "+ (id === option.id));
        }
        for (let variable of this.variables) {
            variable.isActive = (id === variable.id);
            // console.log("Variable "+variable.id + " is active: "+ (id === variable.id));
        }
    }

    replaceTags(text: string): string {
        let replacedTags = text;

        for (let variable of this.variables) {
            let tag: string = variable.tag;
            if (text.indexOf(tag) >= 0) {
                variable.occursInText = true;
            } else {
                variable.occursInText = false;
            }
            if (variable.occursInText && variable.getReplacement() && variable.getReplacement().length > 0) {
                while (text.indexOf(tag) >= 0) {
                    let start: number = text.indexOf(tag);
                    if (start === 0) {
                        text = variable.getReplacement() + text.substring(tag.length);
                    } else {
                        text = text.substring(0, start) + variable.getReplacement() + text.substring(start + tag.length);
                    }
                }
                replacedTags = text;
            }
        }

        return replacedTags;
    }


    isOption(part: any) {
        if (part instanceof Option) {
            // console.log(part + " is an option");
            return true;
        } else {
            return false;
        }
    }

    isVariable(part: any) {
        if (part instanceof Variable) {
            // console.log(part.id + " is variable? " + (part.tag.length > 0));
            return true;
        } else {
            return false;
        }
    }

    isText(part: any) {
        if (part.length > 0 && !part.choices && !this.isVariable(part)) {
            // console.log(part + " is a text");
            return true;
        } else {
            return false;
        }
    }

    isNewline(part: any) {
        if (part.length === 0 && !part.choices) {
            console.log(part + " is a newline");
            return true;
        } else {
            return false;
        }
    }


    /**
     * LOCAL STORAGE to store data in the browser inbetween sessions
     * 
     * This area contains all the functions required for detecting and using local storage
     * to store templates
     */

    storeTemplate(title: string) {
        let localStorage = window.localStorage;
        localStorage.setItem(title, this.templateSource);
        // console.log("Stored " + title + " : " + this.templateSource);
    }

    loadTemplate(title: string): string {
        let text = "";

        let localStorage = window.localStorage;
        text = localStorage.getItem(title);

        return text;
    }

    getTemplateTitles(): Array<string> {
        let titles: Array<string> = [];

        let localStorage = window.localStorage;

        titles = Object.keys(localStorage);

        return titles;
    }

    removeAllTemplates() {
        let localStorage = window.localStorage;
        localStorage.clear();
        console.log("Removed all database entries... - " + localStorage.length);

    }

    removeTemplate(title: string) {
        let localStorage = window.localStorage;
        localStorage.removeItem(title);
    }
}