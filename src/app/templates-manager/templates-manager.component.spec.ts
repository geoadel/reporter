import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplatesManagerComponent } from './templates-manager.component';

describe('TemplatesManagerComponent', () => {
  let component: TemplatesManagerComponent;
  let fixture: ComponentFixture<TemplatesManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplatesManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplatesManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
