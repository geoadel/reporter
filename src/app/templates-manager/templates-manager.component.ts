/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */


import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-templates-manager',
  templateUrl: './templates-manager.component.html',
  styleUrls: ['./templates-manager.component.css']
})
export class TemplatesManagerComponent implements OnInit {


    newTitle : string = "";
    
    constructor(public dataService: DataService) {}

  ngOnInit() {
  }
  
  
    loadTemplate(title : string) {
        if (title && title.length > 0) {
            this.dataService.templateSource = this.dataService.loadTemplate(title);
            this.dataService.templateSourceChanged();
            // console.log("Loaded template " + this.templateTitle + ": " + this.dataService.templateSource);
        }
    }

    storeTemplate(title: string) {
        if (title.length > 0) {
            this.dataService.storeTemplate(title);
        } else {
            // console.log("Title "+title.length+" --  "+text.length);
        }
    }

    updateTemplate() {
        localStorage.setItem("Last template", this.dataService.templateSource);
    }

    getTemplateTitles(): Array<string> {
        return this.dataService.getTemplateTitles()
    }

    removeTemplate(title: string) {
        this.dataService.removeTemplate(title);
    }

}
