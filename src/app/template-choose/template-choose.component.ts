/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */


import {Component, HostListener} from '@angular/core';
import {DataService} from '../data.service';
import {Option} from '../option';
import {Variable} from '../variable';



@Component({
    selector: 'app-template-choose',
    templateUrl: './template-choose.component.html',
    styleUrls: ['./template-choose.component.css']
})

export class TemplateChooseComponent {


    finalText: string = "";

    step: number = 0;
    stepVar: number = 0;

    answered: Array<any> = [];

    @HostListener('click') scrollToActiveElement() {
        var emTag = document.getElementsByTagName("em")[0];
        if (emTag) {
            emTag.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
        }
    }
    
    
    constructor(public dataService: DataService) {}



    setChoice(option: Option, index: number) {
        this.dataService.setChoice(option, index);
    }

    changeSelectedChoices(option: Option, i: number) {
        option.toggleMultipleChoice(i);
        this.dataService.updateMarkdown();
    }

    setActiveOption(part: Option) {
        this.dataService.activate(part);
        console.log("Activating " + part.id);
    }

    // toggle visibility of given option
    update(option: Option) {
        option.toggleDisplay();
        // this.dataService.updateFinalText();
    }

    replaceTags(text: string): string {
        return this.dataService.replaceTags(text);
    }

    getTitle(option: any): string {
        // if the option is a variable
        if (this.dataService.isVariable(option)) {
            return option.tag;
        }
        // if the option has a title
        if (option.getTitle()) {
            let title : string = option.getTitle();
            if(title.length > 24){
                return title.substring(0, 20) + "…";
            }
            return option.getTitle();
        } else {
            return "Choice";
        }
    }

    // for checkboxes, shorten the visible text as not to break the design. The lon version should be made accessible e.g. via tooltips
    getTextShortened(text: string): string {
        let textShort = text;

        if (textShort.length > 31) {
            console.log("Text was too long: " + text);
            textShort = text.substring(0, 30);
        }

        return textShort;
    }

    getVariables(): Array<Variable> {
        // console.log("Returning variables : " + JSON.stringify(this.dataService.variables));
        return this.dataService.variables;
    }

    getOptions(): Array<any> {
        return this.dataService.getTextParts();
    }

    getFinalText(): string {
        return this.dataService.finalText;
    }

    updateFinalText() {
        // this.dataService.updateFinalText();
    }

    updateFocus(id) {
        if (id) {
            this.dataService.setActive(id);
        }
        this.finalText = this.dataService.updateMarkdown();
    }
}
