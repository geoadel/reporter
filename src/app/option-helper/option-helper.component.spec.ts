import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionHelperComponent } from './option-helper.component';

describe('OptionHelperComponent', () => {
  let component: OptionHelperComponent;
  let fixture: ComponentFixture<OptionHelperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionHelperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
