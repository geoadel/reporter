/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */


import {Component, OnInit} from '@angular/core';

import {DataService} from '../data.service';


@Component({
    selector: 'app-option-helper',
    templateUrl: './option-helper.component.html',
    styleUrls: ['./option-helper.component.css']
})
export class OptionHelperComponent implements OnInit {

    types: Array<String> = ["Simple choice", "Multiple choice", "Named choice"];

    selectedType: String = "";

    choices: Array<String> = ["", ""];
    names: Array<String> = ["", ""];
    title: String = "";
    connector: String = "";

    optionText: String = "";

    addChoice() {
        this.choices.push("");
    }

    reset() {
        this.choices = ["", ""];
        this.names = ["", ""];
        this.title = "";
        this.connector = "";
    }

    constructor(public dataService: DataService) {}


    ngOnInit() {
    }

    isSimpleOrMultipleChoice(): Boolean {
        return (this.selectedType === "Simple choice" || this.selectedType === "Multiple choice");
    }

    updateChoice(index: number, choiceText: string) {
        this.choices[index] = choiceText;
        this.updateResult();
    }

    updateResult() {
        let result: String = "";
        console.log(JSON.stringify(this.choices));

        let endingString = "";

        if (this.selectedType === "Simple choice") {
            result = "[";
            endingString = "]";
        }

        if (this.selectedType === "Multiple choice") {
            result = "<";
            endingString = ">";
        }

        if (this.selectedType === "Named choice") {
            result = "{";
            endingString = "}";
        }

        for (let i = 0; i < this.choices.length; i++) {
            if (i < 1 && this.choices.length > 0) {
                if (this.title && this.title.length > 0) {
                    result += this.title + "*";
                }
                if (this.connector.length > 0) {
                    result += this.connector+"%";
                }
                
                if(this.selectedType === "Named choice"){
                    result += this.names[0] + ":";
                }
                
                result += "" + this.choices[0];
            }
            if (i >= 1 && i < this.choices.length - 1) {
                result += "|" 
                
                if(this.selectedType === "Named choice"){
                    result += this.names[i] + ":";
                }
                
                result += ""+ this.choices[i];
                
            }
            if (i === this.choices.length - 1 && this.choices.length > 1) {
                result += "|" 
                
                if(this.selectedType === "Named choice"){
                    result += this.names[i] + ":";
                }
                
                result += ""+ this.choices[i];
            }
        }

        result += endingString;

        this.optionText = result;
    }

    trackById(index: number, obj: any): any {
        return index;
    }

}
