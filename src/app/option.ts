/**
 *  Copyright 2018 Georg Adelmann <mail@geoadel.net>
 *     This file is part of Reporter.

    Reporter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Reporter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Reporter.  If not, see <http://www.gnu.org/licenses/>.
 */

export class Option {
    constructor(id: number, choices: Array<string>, originalString: string) {
        this.setChoices(choices);
        this.id = id;
        this.originalString = originalString;
    }

    // the id, can simply be a number indicating the position in the text (e.g. char position)
    id: number;

    // the different string choices, this option should offer
    private choices: Array<string>;

    // the number of names needs to correspond to the number of choices
    choicesNames: Array<string>;

    private choicesAbbreviated: Array<string> = [];

    // the original string from which the option was created
    originalString: string;

    // if the choices are named, set this boolean to yes
    isNamed: boolean;

    // the number of the selected choice within the choices array 
    selectedChoice: number;

    // for multiple choice, an array of booleans needs to be used, indicating for each of choices, whether they are selected or not
    selectedChoices: Array<boolean> = [];

    // if one of the choices has been selected, mark this hasBeenSelected as true
    hasBeenSelected: boolean;

    // whether the option is one of the currently being edited options or not
    isActive: boolean = false;

    // title
    title: string;

    // an override can be used to override the choices and use a custom input
    override: string = "";

    // overrideVisible toggles visibility of override setting
    overrideVisible: boolean = false;

    // the connector for the last two selected multiple choices
    multipleChoiceConnector: string = ", ";

    // whether this is a multiple choice option, where several choices can be active at the same time
    isMultipleChoice: boolean;

    // isDisplayedFully is used to toggle the display state of the option
    isDisplayedFully: boolean = true;

    replacement: string = "";

    // denotes whether an override text is added to the existing selection or whether (default) it replaces the selected text.
    overrideAdd: boolean = false;

    // for now this is only used with variables, but to make sure that options are shown, the property is added to options as well - could later be used for nested options
    occursInText: boolean = true;


    toggleDisplay() {
        // console.log("Toggling visiblity of option " + this.id + " to " + this.isDisplayedFully);
        this.isDisplayedFully = !this.isDisplayedFully;
    }

    /**
     * Returns the title string, if it exists, otherwise boolean value false
     */
    getTitle(): any {
        if (this.title && this.title.length > 0) {
            return this.title;
        } else {
            return false;
        }
    }



    getSelectedTextAbbreviated(): string {

        if (this.isNamed) {
            if(this.choicesNames && this.choicesNames[this.selectedChoice]){
                return this.choicesNames[this.selectedChoice];
            } else {
                if (this.choices[this.selectedChoice]){
                    return this.choices[this.selectedChoice];
                }
            }
        }

        let text: string = this.getSelectedText();

        text = text.trim();

        if (text.startsWith("*") && text.endsWith("*")) {
            text = text.substring(1, text.length - 1);
        }

        if (text.length > 24) {
            text = text.substring(0, 20) + "…";
        }

        return text;
    }

    getSelectedText(): string {
        let result: string = "";
        // console.log("Is overrideAdd active? " + this.overrideAdd);
        if (this.overrideVisible && !this.overrideAdd) {
            return this.override;
        }

        if (!this.overrideVisible || this.overrideAdd) {
            if (this.isMultipleChoice) {
                result = this.getMultipleChoiceText();
            } else {
                result = this.getSelectedChoiceText();
            }
        }

        if (this.overrideAdd && this.overrideVisible) {
            result = result + this.override;
        }

        if (this.isActive && result.length > 0) {
            result = result.trim();
            // necessary for the focus scrollToElement hack
            result = "*" + result + "*";
        }

        return result;
    }


    private getSelectedChoiceText() {
        if (this.selectedChoice) {
            return this.choices[this.selectedChoice];
        } else {
            return this.choices[0];
        }
    }

    private getMultipleChoiceText(): string {
        if (this.isMultipleChoice) {
            let result: string = "";
            let i: number = 0;

            let selection: Array<string> = [];
            for (let choice of this.choices) {
                if (this.selectedChoices[i]) {
                    selection.push(choice);
                }
                i++;
            }

            // console.log("Selected " + selection + " in option " + this.title);

            i = 0;
            for (let choice of selection) {
                if (i === 0) {
                    result = choice;
                }
                if (i > 0 && i < selection.length - 1) {
                    result = result + ", " + choice;
                }
                if (i > 0 && i === selection.length - 1) {
                    // if the override should be added to the selection, omit the connector
                    if (this.overrideAdd && this.override.length > 0) {
                        result = result + ", " + choice;
                    } else {
                        result = result + this.multipleChoiceConnector + choice;
                    }
                }
                i++;
            }
            return result;
        }
    }

    // get the string for choices[i]
    getChoice(i: number): string {
        if (this.choices.length > i) {
            return this.choices[i];
        }
    }

    getChoiceAbbreviated(i: number): string {
        // console.log("Return abbreviated choice " + i + ": " + this.choicesAbbreviated[i]);
        if (this.choicesAbbreviated.length > i) {
            return this.choicesAbbreviated[i];
        }
    }

    getMultipleChoiceSelected(i: number): boolean {
        return this.selectedChoices[i];
    }

    // get all choices as Array<string>
    getChoices(): Array<string> {
        if (this.choicesNames && this.choices.length === this.choicesNames.length) {
            return this.choicesNames;
        }
        return this.choices;
    }

    /**
     * If a selection has been made, return that number.
     * If not, return 0 (the first choice of the option is treated as default value)
     */
    getSelectedChoice(): number {
        if (this.selectedChoice) {
            return this.selectedChoice;
        } else {
            return 0;
        }
    }

    toggleMultipleChoice(i: number): boolean {
        if (this.selectedChoices.length > i) {
            this.selectedChoices[i] = !this.selectedChoices[i];
            return this.selectedChoices[i];
        }
        return false;
    }

    isSelected(i: number): boolean {
        return (i === this.getSelectedChoice() && this.hasBeenSelected);
    }

    /**
     * Get the name of the choice. If no name exists, return the choice
     */
    getName(i: number): string {
        if (this.choicesNames && this.choicesNames.length > i) {
            return this.choicesNames[i];
        } else {
            return this.getChoice(i);
        }
    }

    setChoice(i: number) {
        if (this.isMultipleChoice) {
            if (this.selectedChoices.length > i) {
                this.selectedChoices[i] = !this.selectedChoices[i];
            }
        } else {
            if (this.choices.length > i) {
                this.selectedChoice = i;
                this.hasBeenSelected = true;
            }
        }
    }

    setChoices(choices: Array<string>) {
        let trimmedChoices: Array<string> = [];
        for (let choice of choices) {
            trimmedChoices.push(choice.trim());
        }
        this.choices = trimmedChoices;
        this.choicesAbbreviated = this.getAbbreviatedArray(trimmedChoices);
    }

    setChoicesNames(choicesNames: Array<string>) {
        this.choicesNames = choicesNames;
    }

    getAbbreviatedArray(array: Array<string>): Array<string> {
        let result: Array<string> = [];
        for (let i = 0; i < array.length; i++) {
            result.push(this.getAbbreviated(array[i]));
        }
        return result;
    }

    getAbbreviated(text: string): string {
        if (text.length > 32) {
            text = text.substring(0, 30) + "...";
        }
        return text;
    }

    toString(): string {
        return "id: " + this.id + "\n" + "choices: " + this.choices + "\n" + "isNamed: " + this.isNamed + "\n" + "names: " + this.choicesNames + "\n selectedChoice: " + this.selectedChoice;
    }

    identicalToString(text: string): boolean {
        return this.originalString === text;
    }


    // if this option and the option given as parameter are identical and the title is identical, the options are identical
    identicalToOption(option: Option): boolean {
        let choicesIdentical: boolean = option.getChoices().toString() === this.getChoices().toString();
        let titlesIdentical: boolean = option.getTitle() === this.getTitle();
        // console.log("Choices " + option.id + " & " + this.id + " are identical: " + choicesIdentical + " ----- titles are identical: " + titlesIdentical + " +++ Verdict: " + (choicesIdentical && titlesIdentical));
        return (choicesIdentical && titlesIdentical);
    }
}


